# Flux Query Builder Docs

## Functions &raquo; addRangeFrom()

### Parameters:

<table>
  <tbody>
    <tr>
      <th>Name</th>
      <th>Required</th>
      <th>Data type</th>
      <th>Description</th>
    </tr>
    <tr>
      <td>from</td>
      <td>Yes</td>
      <td>string</td>
      <td>Relative duration.</td>
    </tr>
  </tbody>
</table>


### Example

```php
->addRangeFrom( '-1h' )
```

This will result in the following Flux function part:

```
|> range(
  start: -1h
)
```

### Extra resources

* [Flux documentation](https://docs.influxdata.com/flux/v0.x/stdlib/universe/range/)