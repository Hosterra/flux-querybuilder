[![influxdb](https://badgen.net/badge/influxdb/2.x/blue)](https://hosterra.dev/Hosterra/netbox-php)
[![php](https://badgen.net/badge/php/8.2+/purple)](https://hosterra.dev/Hosterra/netbox-php)
[![license](https://badgen.net/badge/license/MIT/green)](https://hosterra.dev/Hosterra/netbox-php/src/branch/main/LICENSE.md)

# Flux Query Builder

Flux Query Builder is a package to build [Flux](https://www.influxdata.com/products/flux/) queries in PHP.

>Note: this package is a fork of the David Arendsen [fluxquerybuilder](https://github.com/davidarendsen/fluxquerybuilder) package; original credits go to him.

## Documentation 
The documentation of this project is available in the [docs folder](docs/00-index.md).

## Contributing
You're welcome to contribute to this project, but please add tests.

### Testing
To execute the testing suite:

```
composer test
```
