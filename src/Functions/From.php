<?php

namespace Hosterra\FluxBuilder\Functions;

use Hosterra\FluxBuilder\Type;

class From extends Base {
	/**
	 * @var array $settings
	 */
	private $settings;

	public function __construct( array $settings ) {
		$this->settings = $settings;
	}

	public function __toString() {
		return 'from(' . new Type( $this->settings ) . ') ';
	}
}
