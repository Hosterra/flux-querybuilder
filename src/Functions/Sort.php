<?php

namespace Hosterra\FluxBuilder\Functions;

use Hosterra\FluxBuilder\Type;

class Sort extends Base {
	/**
	 * @var array $columns
	 */
	private $columns;

	/**
	 * @var bool $desc
	 */
	private $desc;

	public function __construct( array $columns = [ '_value' ], bool $desc = false ) {
		$this->columns = $columns;
		$this->desc    = $desc;
	}

	public function __toString() {
		return '|> sort(columns: [' . new Type( $this->columns ) .
		       '], desc: ' . new Type( $this->desc ) . ') ';
	}
}
