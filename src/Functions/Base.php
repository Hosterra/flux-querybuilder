<?php

namespace Hosterra\FluxBuilder\Functions;

use Hosterra\FluxBuilder\Exception\FunctionNotImplementedException;

abstract class Base {
	/**
	 * @throws FunctionNotImplementedException
	 */
	public function __toString() {
		throw new FunctionNotImplementedException( '__toString', get_class( $this ) );
	}
}
