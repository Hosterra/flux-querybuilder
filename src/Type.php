<?php

namespace Hosterra\FluxBuilder;

use Hosterra\FluxBuilder\Type\TypeInterface;
use Hosterra\FluxBuilder\Type\ArrayType;
use Hosterra\FluxBuilder\Type\BooleanType;
use Hosterra\FluxBuilder\Type\TimeType;
use DateTime;

class Type {
	/**
	 * @var mixed $value
	 */
	protected $value;

	public function __construct( $value ) {
		$this->value = $value;
	}

	public function __toString(): string {
		switch ( gettype( $this->value ) ) {
			case 'object':
				if ( $this->value instanceof DateTime ) {
					return new TimeType( $this->value );
				}

				return $this->value->__toString();
			case 'string':
				return '"' . $this->value . '"';
			case 'boolean':
				return new BooleanType( $this->value );
			case 'array':
				return new ArrayType( $this->value );
			case 'NULL':
				return 'null';
			default:
				return (string) $this->value;
		}
	}
}
