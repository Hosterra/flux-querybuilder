<?php

namespace Hosterra\FluxBuilder\Exception;

use Exception;

class FunctionNotImplementedException extends Exception {
}
