<?php

namespace Hosterra\FluxBuilder\Exception;

use Exception;

class FunctionRequiredSettingMissingException extends Exception {
	public function __construct( string $functionName, string $message ) {
		parent::__construct( 'Function ' . $functionName . ' - ' . $message );
	}
}
