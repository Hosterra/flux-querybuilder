<?php

namespace Hosterra\FluxBuilder\Exception;

use Exception;

class FunctionInvalidInputException extends Exception {
}
