<?php

namespace Hosterra\FluxBuilder\Exception;

use Exception;

class ExpressionNotImplementedException extends Exception {
}
