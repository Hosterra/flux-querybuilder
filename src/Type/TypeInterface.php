<?php

namespace Hosterra\FluxBuilder\Type;

interface TypeInterface {
	public function __toString(): string;
}
