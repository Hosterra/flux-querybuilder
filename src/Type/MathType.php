<?php

namespace Hosterra\FluxBuilder\Type;

class MathType implements TypeInterface {
	public function __construct( string $value ) {
		$this->value = $value;
	}

	public function __toString(): string {
		return $this->value;
	}
}
