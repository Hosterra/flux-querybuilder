<?php

namespace Hosterra\FluxBuilder\Expression;

use Hosterra\FluxBuilder\Exception\ExpressionNotImplementedException;

abstract class Base {
	/**
	 * @throws ExpressionNotImplementedException
	 */
	public function __toString() {
		throw new ExpressionNotImplementedException( '__toString', get_class( $this ) );
	}
}
