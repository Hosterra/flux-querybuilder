<?php

namespace Hosterra\FluxBuilder\Builder;

interface QueryBuilderInterface {
	public function build(): string;
}
