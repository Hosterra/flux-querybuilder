<?php

namespace Hosterra\FluxBuilder\Builder;

use DateTime;
use Hosterra\FluxBuilder\QueryBuilder;
use Hosterra\FluxBuilder\Builder\QueryBuilderInterface;
use Hosterra\FluxBuilder\Expression\KeyFilter;
use Hosterra\FluxBuilder\Functions\Filter;
use Hosterra\FluxBuilder\Functions\From;
use Hosterra\FluxBuilder\Functions\Measurement;
use Hosterra\FluxBuilder\Functions\Range;
use Hosterra\FluxBuilder\Functions\RawFunction;

trait Basics {
	public function from( array $from ): QueryBuilderInterface {
		$this->setRequirements();

		$this->addToQuery(
			new From( $from )
		);

		return $this;
	}

	public function fromBucket( string $bucket ): QueryBuilderInterface {
		return $this->from( [ 'bucket' => $bucket ] );
	}

	public function fromMeasurement( string $measurement ): QueryBuilderInterface {
		$this->setRequirements();

		$this->addToQuery(
			new Measurement( $measurement )
		);

		return $this;
	}

	public function addKeyFilter( KeyFilter $keyFilter ): QueryBuilderInterface {
		$this->setRequirements();

		$this->addToQuery(
			new Filter( $keyFilter )
		);

		return $this;
	}

	public function addFieldFilter( array $fields ): QueryBuilderInterface {
		$this->setRequirements();

		$this->addToQuery(
			new Filter( $fields )
		);

		return $this;
	}

	public function addRange( mixed $start, ?DateTime $stop = null ): QueryBuilderInterface {
		$this->setRequirements();

		$this->addToQuery(
			new Range( $start, $stop )
		);

		return $this;
	}

	public function addRangeStart( DateTime $start ): QueryBuilderInterface {
		$this->addRange( $start );

		return $this;
	}

	public function addRangeFrom( string $from ): QueryBuilderInterface {
		$this->addRange( $from );

		return $this;
	}

	public function addRangeInBetween( DateTime $start, DateTime $stop ): QueryBuilderInterface {
		$this->addRange( $start, $stop );

		return $this;
	}

	public function addRawFunction( string $input ): QueryBuilderInterface {
		$this->addToQuery(
			new RawFunction( $input )
		);

		return $this;
	}

	private function setRequirements() {
		$this->requiredFluxQueryParts = [
			From::class,
			Range::class,
			Measurement::class,
		];
	}
}
